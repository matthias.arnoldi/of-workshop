/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  4.x                                   |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      controlDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

application     icoFoam;

startFrom       startTime;

startTime       0;

stopAt          endTime;

endTime         120;

deltaT          0.01;

writeControl    timeStep;

writeInterval   10;

purgeWrite      0;

writeFormat     ascii;

writePrecision  8;

writeCompression off;

timeFormat      general;

timePrecision   6;

runTimeModifiable true;

// ************************************************************************* //

functions
{

///////////////////////////////////////////////////////////////////////////

	minmaxdomain
	{
		type fieldMinMax;

		functionObjectLibs ("libfieldFunctionObjects.so");

		enabled true; //true or false

		mode component;

		writeControl timeStep;
		writeInterval 1;

		log true;

		fields (p U s1 s2 s3);
	}

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

    massFlow_inlet1
    {
        type            surfaceRegion;
        functionObjectLibs ("libfieldFunctionObjects.so");
        enabled         true;

		//writeControl     outputTime;
		writeControl   timeStep;
		writeInterval  1;

        log             true;

        writeFields     false;

        regionType      patch;
        name      		inlet1;

		operation       sum;
        fields
        (
            phi
        );
    }

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

    massFlow_inlet2
    {
        type            surfaceRegion;
        functionObjectLibs ("libfieldFunctionObjects.so");
        enabled         true;

		//writeControl     outputTime;
		writeControl   timeStep;
		writeInterval  1;

        log             true;

        writeFields     false;

        regionType      patch;
        name      		inlet2;

		operation       sum;
        fields
        (
            phi
        );
    }

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

    massFlow_outlet
    {
        type            surfaceRegion;
        functionObjectLibs ("libfieldFunctionObjects.so");
        enabled         true;
	
		//writeControl     outputTime;
		writeControl   timeStep;
		writeInterval  1;

        log             true;

        writeFields     false;

        regionType      patch;
        name    		outlet;

		operation       sum;
        fields
        (
            phi
        );
    }

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

	inlet1_WA
	{
		type            surfaceRegion;
		functionObjectLibs ("libfieldFunctionObjects.so");
		enabled         true;

		writeControl   timeStep;
		writeInterval  1;

		log             true;

		writeFields     false;

		regionType      patch;
		name      		inlet1;

		operation       weightedAverage;

		fields
		(
		    phi
		    U
			p
		);
	}

	inlet2_WA
	{
		type            surfaceRegion;
		functionObjectLibs ("libfieldFunctionObjects.so");
		enabled         true;

		writeControl   timeStep;
		writeInterval  1;

		log             true;

		writeFields     false;

		regionType      patch;
		name      		inlet2;

		operation       weightedAverage;

		fields
		(
		    phi
		    U
			p
		);
	}

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////

	inlet1Max
	{
		type            surfaceRegion;
		functionObjectLibs ("libfieldFunctionObjects.so");
		enabled         true;

		writeControl   timeStep;
		writeInterval  1;

		log             true;

		writeFields     false;

		regionType      patch;
		name      		inlet1;

		operation       max;

		fields
		(
		    U
			p
			s1
			s2
			s3
		);
	}

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

	inlet2Max
	{
		type            surfaceRegion;
		functionObjectLibs ("libfieldFunctionObjects.so");
		enabled         true;

		writeControl   timeStep;
		writeInterval  1;

		log             true;

		writeFields     false;

		regionType      patch;
		name      		inlet2;

		operation       max;

		fields
		(
		    U
			p
			s1
			s2
			s3
		);
	}

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

	outletMax
	{
		type            surfaceRegion;
		functionObjectLibs ("libfieldFunctionObjects.so");
		enabled         true;

		writeControl   timeStep;
		writeInterval  1;

		log             true;

		writeFields     false;

		regionType      patch;
		name      		outlet;

		operation       max;

		fields
		(
		    U
			p
			s1
			s2
			s3
		);
	}

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

	scalar1
	{
		type            scalarTransport;
		functionObjectLibs ("libsolverFunctionObjects.so");

		enabled true;

		writeControl outputTime;
		//writeControl timeStep;
		//writeInterval 1;

		log yes;

		nCorr 1;

		//difussion coefficient
		D 0.0001;

		//name of field
		field s1;

		//use the schemes of field, in this case, U
		//schemesField U;

	}

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

	scalar2
	{
		type            scalarTransport;
		functionObjectLibs ("libsolverFunctionObjects.so");

		enabled true;

		writeControl outputTime;
		//writeControl timeStep;
		//writeInterval 1;

		log yes;

		nCorr 2;

		//difussion coefficient
		D 0.001;

		//name of field
		field s2;

		//use the schemes of field, in this case, U
		//schemesField U;

	}

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////

	scalar3
	{
		type scalarTransport;
		functionObjectLibs ("libutilityFunctionObjects.so");

		enabled true;

		writeControl outputTime;
		//writeControl timeStep;
		//writeInterval 1;

		log yes;

		nCorr 2;

		//difussion coefficient
		DT 0.01;

		//name of field
		field s3;

		fvOptions
		{
		    scalar3
			{
			    type            scalarFixedValueConstraint;
		        active          true;

	   	    	scalarFixedValueConstraintCoeffs 
			    {
		            timeStart       1;
		    		duration        8;

		    		selectionMode   cellZone;
					cellZone	    source1;

					fieldValues
		    		{
		        	    s3 3;
		    		}
		    	}
			}
		}
	}

///////////////////////////////////////////////////////////////////////////

};
